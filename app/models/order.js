/**
 * Created by Ashok on 01-05-2015.
 */
mongoose = require("mongoose");
Schema = mongoose.Schema;

var OrderSchema = new Schema({
    product_name: String,
    quantity: Number,
    created: {type: Date, default: Date.now},
    status: {type: String, enum: ['new', 'accepted', 'rejected'], default: 'new'}
});

module.exports = mongoose.model('Order', OrderSchema);


