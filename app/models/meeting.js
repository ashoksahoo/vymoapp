/**
 * Created by Ashok on 01-05-2015.
 */
mongoose = require("mongoose");
Schema = mongoose.Schema;

var MeetingSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    date: {
        type: Date
    },
    notes: {
        type: String,
        default: '',
        trim: true
    },
    new_orders: [{
        type: Schema.Types.ObjectId,
        ref: 'Order'
    }],
    payments: [{
        type: Schema.Types.ObjectId,
        ref: 'Payment'
    }]

});

module.exports = mongoose.model('Meeting', MeetingSchema);



