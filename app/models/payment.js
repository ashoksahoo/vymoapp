/**
 * Created by Ashok on 01-05-2015.
 */
mongoose = require("mongoose");
Schema = mongoose.Schema;

var PaymentSchema = new Schema({
    id: String,
    type: String,
    amount: Number,
    created: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Payment', PaymentSchema);


