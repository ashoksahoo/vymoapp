var Meeting = require('../models/meeting');
var Order = require('../models/order');
var Payment = require('../models/payment');
var async = require('async');
var _ = require('lodash');

exports.findMeetingById = function(meetingId, callback) {
    Meeting.findOne({_id: meetingId}, function(err, obj) {
        if(err) {
            console.error(err);
            callback(err.message);
        }
        else {
            callback(null, obj)
        }
    });

};


exports.createMeeting = function(record, appCallback) {
    async.parallel({
        orders: function(callback) {
            Order.create(record.orders, callback)
        },
        payments: function(callback) {
            Payment.create(record.payments, callback)
        }

    }, function(err, results) {
        record.new_orders = results.orders;
        record.payments = results.payments;
        Meeting.create(record, appCallback)
    });

};

exports.findAllMeetings = function(callback) {
    Meeting.find({})
        .populate('new_orders')
        .populate('payments')
        .exec(function(err, obj) {
            if(err) {
                callback(err);
            }
            else {
                callback(null, obj);
            }
        })
};

exports.getOrderUpdates = function(callback) {

    Order.find({$or: [{'status': "accepted"}, {'status': "rejected"}]}, callback);
};
exports.getOrders = function(callback) {
    Order.find({'status': "new"}, callback);
};
exports.getPayments = function(callback) {
    Payment.find({}, callback);
};
exports.approveOrder = function(id, callback) {

    Order.findById(id, function(err, obj) {
        if(err) {
            return callback(err);
        }
        obj.status = 'accepted';
        obj.save(callback)
    });
};
exports.rejectOrder = function(id, callback) {

    Order.findById(id, function(err, obj) {
        if(err) {
            return callback(err);
        }
        obj.status = 'rejected';
        obj.save(callback)
    });
};
