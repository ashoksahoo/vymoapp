var meetingService = require('../services/meeting');

exports.listAllMeetings = function(req, res) {
    var callback = function(err, obj) {
        if(err)
            res.send(500, err);
        else
            res.send(obj)
    };
    meetingService.findAllMeetings(callback)
};

exports.createNewMeeting = function(req, res) {
    var callback = function(err, obj) {
        if(err)
            res.status(500).send(err);
        else
            res.send(obj)
    };
    var record = {};
    record.notes = req.body.notes;
    record.orders = req.body.orders;
    record.payments = req.body.payments;
    record.date = req.body.date;
    meetingService.createMeeting(record, callback)

};

exports.getOrderUpdates = function(req, res) {
    var callback = function(err, obj) {
        if(err)
            res.send(500, err);
        else
            res.send(obj)
    };
    meetingService.getOrderUpdates(callback)
};

exports.getOrders = function(req, res) {
    var callback = function(err, obj) {
        if(err)
            res.send(500, err);
        else
            res.send(obj)
    };
    meetingService.getOrders(callback)
};
exports.getPayments = function(req, res) {
    var callback = function(err, obj) {
        if(err)
            res.send(500, err);
        else
            res.send(obj)
    };
    meetingService.getPayments(callback)
};
exports.approveOrder = function(req, res) {
    var orderId = req.params.id;
    var callback = function(err, obj) {
        if(err)
            res.send(500, err);
        else
            res.send(obj)
    };
    meetingService.approveOrder(orderId,callback)
};
exports.rejectOrder = function(req, res) {
    var orderId = req.params.id;
    var callback = function(err, obj) {
        if(err)
            res.send(500, err);
        else
            res.send(obj)
    };
    meetingService.rejectOrder(orderId,callback)
};

