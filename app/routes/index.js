var express = require('express');
var controller = require('../controllers/meeting');
var router = express.Router();

/* GET home page. */
router.get('/meeting', controller.listAllMeetings);
router.post('/meeting', controller.createNewMeeting);
router.get('/order-updates', controller.getOrderUpdates);
router.get('/orders', controller.getOrders);
router.get('/payments', controller.getPayments);
router.get('/approve-order/:id', controller.approveOrder);
router.get('/reject-order/:id', controller.rejectOrder);

module.exports = router;
