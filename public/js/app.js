/**
 * Created by Ashok on 7/27/2014.
 */

'use strict';

var url = "http://localhost:3000/";

angular.module('vymoApp', [
  'ngRoute',
  'vymoApp.services',
  'vymoApp.controllers'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/meeting', {templateUrl: 'tpl/meeting-list.html', controller: 'MeetingListCtrl'});
  $routeProvider.when('/meeting/new', {templateUrl: 'tpl/create-meeting.html', controller: 'CreateMeetingCtrl'});
  $routeProvider.when('/order-updates', {templateUrl: 'tpl/order-updates.html', controller: 'OrderUpdatesCtrl'});
  $routeProvider.when('/orders', {templateUrl: 'tpl/order-list.html', controller: 'OrderListCtrl'});
  $routeProvider.when('/payments', {templateUrl: 'tpl/payment-list.html', controller: 'PaymentListCtrl'});
  $routeProvider.otherwise({redirectTo: '/meeting'});
}]);
