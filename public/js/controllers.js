/**
 * Created by Ashok on 7/27/2014.
 */
'use strict';

/* Controllers */

angular.module('vymoApp.controllers', [])
    .controller('CreateMeetingCtrl', ['$scope', '$rootScope', '$http', '$location',
        function($scope, $rootScope, $http, $location) {
            angular.extend($scope, {
                notes: '',
                payments: [],
                orders: []
            });
            $scope.addListItem = function(type) {
                switch(type) {
                    case "order":
                        $scope.orders.push($.extend({}, newOrder));
                        break;
                    case "payment":
                        $scope.payments.push($.extend({}, newPayment));
                        break;
                }
            };
            $scope.removeListItem = function(type, index) {
                switch(type) {
                    case "order":
                        $scope.orders.splice(index, 1);
                        break;
                    case "payment":
                        $scope.payments.splice(index, 1);
                        break;
                }
            };
            var newOrder = {
                product_name: '',
                quantity: ''
            };
            var newPayment = {
                type: '',
                amount: ''
            };

            $scope.post = function() {
                var meeting = {
                    notes: this.notes,
                    orders: this.orders,
                    payments: this.payments
                };
                $http.post(url + 'meeting', meeting)
                    .success(function(response) {
                        $scope.post = response;
                        $location.url('/');
                    })
                    .error(function() {
                        $scope.msg = 'Can not Create Meetings';
                    });

            };


        }])
    .controller('MeetingListCtrl', ['$scope', '$rootScope', '$http', '$location',
        function($scope, $rootScope, $http, $location) {
            $scope.createPage = function() {
                $location.path('/meeting/new')
            };
            $scope.orderUpdatePage = function() {
                $location.path('/order-updates')
            };
            $http.get(url + 'meeting')
                .success(function(response) {
                    $scope.meetings = response;
                })
                .error(function() {
                    $scope.msg = 'Can not Fetch Meetings';
                }
            );
        }
    ])
    .controller('OrderUpdatesCtrl', ['$scope', '$rootScope', '$http', '$location',
        function($scope, $rootScope, $http) {
            $http.get(url + 'order-updates')
                .success(function(response) {
                    $scope.orders = response;
                })
                .error(function() {
                    $scope.msg = 'Can not Fetch Meetings';
                }
            );
        }
    ])
    .controller('OrderListCtrl', ['$scope', '$rootScope', '$http', '$location',
        function($scope, $rootScope, $http) {
            $scope.approveOrder = function(orderId) {
                $http.get(url + 'approve-order/' + orderId)
                    .success(function() {
                        getOrders();
                    })
                    .error(function() {
                        $scope.msg = 'Can not approve order';
                    }
                );
            };
            $scope.rejectOrder = function(orderId) {
                $http.get(url + 'reject-order/' + orderId)
                    .success(function() {
                        getOrders();
                    })
                    .error(function() {
                        $scope.msg = 'Can not reject Order';
                    }
                );
            };
            var getOrders = function() {
                $http.get(url + 'orders')
                    .success(function(response) {
                        $scope.orders = response;
                    })
                    .error(function() {
                        $scope.msg = 'Can not Fetch Order';
                    }
                );
            };
            getOrders();
        }
    ])
    .controller('PaymentListCtrl', ['$scope', '$rootScope', '$http', '$location',
        function($scope, $rootScope, $http) {
            var getPayments = function() {
                $http.get(url + 'payments')
                    .success(function(response) {
                        $scope.payments = response;
                    })
                    .error(function() {
                        $scope.msg = 'Can not Fetch Order';
                    }
                );
            };
            getPayments();
        }
    ]);
